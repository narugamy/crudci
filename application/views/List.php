<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Lista</title>
		<link rel="stylesheet" href="<?=base_url()?>assets/librerias/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/librerias/bower_components/datatables.net-bs/css/dataTables.bootstrap.css">
		<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
	</head>
	<body>
	<div class="header"></div>
	<div class="container">
		<h2>Lista de Usuarios</h2>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID</th>
					<th>Email</th>
					<th>Nombre</th>
					<th>Apellidos</th>
					<th>Fecha de Nacimiento</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($users as $user):?>
					<tr>
						<td><?= $user->id ?></td>
						<td><?= $user->email ?></td>
						<td><?= $user->name ?></td>
						<td><?= $user->surnames ?></td>
						<td><?= $user->date ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
	<div class="footer">
		<footer>
		
		</footer>
	</div>
	<script src="<?=base_url()?>assets/librerias/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	<script>
		$(document).on('ready',function() {
			$('.table').DataTable({
					"lengthMenu": [[5, 10, 15, -1], [5, 10, 15, "All"]],
					"language": {
						"sProcessing":     "Procesando...",
						"sLengthMenu":     "Mostrar _MENU_ registros",
						"sZeroRecords":    "No se encontraron resultados",
						"sEmptyTable":     "Ningún dato disponible en esta tabla",
						"sInfo":           "Mostrando registros del _START_ al _END_",
						"sInfoEmpty":      "No se ha encontrado ningun registro",
						"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
						"sInfoPostFix":    "",
						"sSearch":         "Buscar: ",
						"sUrl":            "",
						"sInfoThousands":  ",",
						"sLoadingRecords": "Cargando...",
						"oPaginate": {
							"sFirst":    "Primero",
							"sLast":     "Último",
							"sNext":     "Siguiente",
							"sPrevious": "Anterior"
						},
						"oAria": {
							"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
							"sSortDescending": ": Activar para ordenar la columna de manera descendente"
						}
					}
				});
		});
	</script>
	</body>
</html>