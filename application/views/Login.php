<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
</head>
<body>
<div class="header"></div>
<div class="container">
	<?=form_open('Login',['class'=>'form'])?>
	<div class="form-group">
		<?=form_label('Correo Electronico','email')?>
		<?=form_input(['name'=>'email','type'=>'email','required'=>'','class'=>'form-control','id'=>'email','value'=>set_value('email')])?>
	</div>
	<div class="form-group">
	 <?=form_error('email')?>
	</div>
	<div class="form-group">
		<?=form_label('Contraseña','password')?>
		<?=form_password(['name'=>'password','required'=>'','class'=>'form-control','id'=>'password','value'=>set_value('password')])?>
	</div>
	<div class="form-group">
		<?=form_error('password')?>
	</div>
	<div class="form-group btn-form">
		<?=form_button(['class'=>'btn btn-success btn-lg','type'=>'submit'],'Registrar')?>
	</div>
	<?=form_close()?>
</div>
<div class="footer">
	<footer>
	
	</footer>
</div>
<script src="<?=base_url()?>assets/librerias/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url()?>assets/librerias/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/librerias/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?=base_url()?>assets/librerias/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
</body>
</html>