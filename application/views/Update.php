<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="<?=base_url()?>assets/librerias/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
</head>
<body>
	<div class="header"></div>
	<div class="container">
		<?=form_open('Home/updated',['class'=>'form'])?>
		<?php foreach ($users as $user):?>
		<div class="form-group">
			<?=form_hidden('id',$user->id)?>
		</div>
		<div class="form-group">
			<?=form_label('Nombre','name')?>
			<?=form_input(['name'=>'name','required'=>'','class'=>'form-control','id'=>'name','value'=>$user->name])?>
		</div>
		<div class="form-group">
			<?=form_label('Apellidos','surnames')?>
		<?=form_input(['name'=>'surnames','required'=>'','class'=>'form-control','id'=>'surnames','value'=>$user->surnames])?>
		</div>
		<div class="form-group">
			<?=form_label('Contraseña','password')?>
			<?=form_password(['name'=>'password','required'=>'','class'=>'form-control','id'=>'password','value'=>$user->password])?>
		</div>
		<div class="form-group">
			<?=form_label('Fecha de Nacimiento','date')?>
			<?=form_input(['name'=>'date','type'=>'date','required'=>'','class'=>'form-control','id'=>'date','value'=>$user->date])?>
		</div>
		<div class="form-group">
				<?=form_button(['class'=>'btn btn-success btn-lg','type'=>'submit'],'Registrar')?>
		</div>
		<?php endforeach;?>
		<?=form_close()?>
	</div>
	<div class="footer">
		<footer>
			
		</footer>
	</div>
	<script src="<?=base_url()?>assets/librerias/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?=base_url()?>assets/librerias/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>