<?php

class Modeluser extends CI_Model	{

	public function __construct(){
		// Call the CI_Model constructor
		parent::__construct();
	}

	public function get($array=null){
		if(!empty($array)):
			$this->db->where($array);
		endif;
		$query=$this->db->get('users');
		return $query->result();
	}

	public function delete($id){
		$this->db->where($id);
		$query=$this->db->delete('users');
		return $query;
	}

	public function update($id,$array){
		$this->db->where($id);
		$query=$this->db->update('users',$array);
		return $query;
	}

	public function insert($array){
		$query=$this->db->insert('users',$array);
		return $query;
	}

}