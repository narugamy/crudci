<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Modeluser','User');
	}

	public function Index(){
		$this->load->view('Login');
	}

	public function Delete($id=null){
		if(is_numeric($id)):
			$us=['id'=>$id];
			echo $this->User->delete($us);
		else:
			echo "El id mandado no es numerico por favor ingrese otro valor";
		endif;
	}

	public function Create(){
		$this->load->view('Create');
	}
	
	public function Register(){
		$user=$this->input->post(null,true);
		$user['type_id']=1;
		$user=$this->Date($user);
		if($this->User->insert($user)==1):
			echo "Registro Exitoso";
		else:
			echo "Se sucito un error en el registro";
		endif;
	}
	
	public function Update($id){
		if(is_numeric($id)):
			$user['users']=$this->User->get(['id'=>$id]);
			$this->load->view('Update',$user);
		else:
			echo "EL usuario o existe";
		endif;
	}
	
	public function Updated(){
		$user=$this->input->post(null,true);
		$id=$user['id'];
		unset($user['id']);
		if($this->User->update(['id'=>$id],$user)):
			$msg="Actualizacion Existosa";
		else:
			$msg="Se sucito un error en la actualizacion";
		endif;
		echo $msg;
	}
	
	private function Date($array){
		$array['created_at']=date('Y-m-d H:i:s');
		$array['updated_at']=date('Y-m-d H:i:s');
		return $array;
	}
	
	public function Login(){
		$user=$this->input->post(null,true);
		$this->Validation();
		if ($this->form_validation->run() == FALSE):
			$this->load->view('Login');
		else:
			$this->load->view('Index');
		endif;
	}
	
	public function Listar(){
		$users['users']=$this->User->get();
		$this->load->view('List',$users);
	}
	
	private function Validation(){
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');
	}
}
